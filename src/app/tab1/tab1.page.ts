import { Component } from '@angular/core';
import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import { TrainLocationsService } from '../train-locations.service';
import { TrainStationsService } from '../train-stations.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  trains: any = [];
  stations: any = [];
  markers: any = [];
  updateTime: number = 30000; // time in ms 

  constructor(private trainLocationService: TrainLocationsService, private traisStationService: TrainStationsService){}

  ngOnInit() {
    this.trainLocationService.getLocations().subscribe( data => {
      this.trains = data;
      this.buildMap();
    });
  }

  buildMap() {
    //console.log("building map");
    mapboxgl.accessToken = 'pk.eyJ1Ijoic2FtcHNhZGV2IiwiYSI6ImNqc3RmYWN0bDFpM3I0NG16Y21qcDIyMngifQ.W5KKSOaQa7uMwzcf0x6i1Q';
    var map = new mapboxgl.Map({
      //style: 'mapbox://styles/mapbox/light-v9',
      style: 'mapbox://styles/sampsadev/cjtzluie9035f1fph2w40tzh7',
      center: [25.93545, 65.16952],
      zoom: 3.9, // 3.9
      pitch: 0,
      minZoom: 3.9, // 3.9
      maxZoom: 17,
      container: 'map'
    });

    this.initMarkers(map);
    this.addStations(map);

    // this is where we enable the locationbutton, but couldnt make it work on 
    // native build. fix if we have time.
    /*
    map.addControl(new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      trackUserLocation: true
    }));
    */
  }

  initMarkers(map) {
    var numOfTrains = this.trains.length;
    this.createMarkers(map);

    var interval = setInterval(() => {
      this.trainLocationService.getLocations().subscribe( data => {
        this.trains = data;
        
        for (let i = 0; i < this.trains.length; i++) {
          if(numOfTrains != this.trains.length) {
            clearInterval(interval);
            this.removeOldMarkers();
            this.initMarkers(map);
            break;
          }
          this.updateMarkers(map, i);
        }
      }); 
    }, this.updateTime);
  }

  // create markers for the train locations
  createMarkers(map) {
    for (let i = 0; i < this.trains.length; i++) {
      // create the popup
      var popup = new mapboxgl.Popup({ offset: 25 })
      //.setText(this.trains[i].trainNumber);
      .setHTML(
        '<h5> Train number: ' + this.trains[i].trainNumber + '</h5>'
      );
      
      // create DOM element for the marker
      var el = document.createElement('div');
      el.id = 'marker';

      var marker = new mapboxgl.Marker(el)
      .setLngLat([
        this.trains[i].location.coordinates[0],
        this.trains[i].location.coordinates[1]
      ])
      .setPopup(popup) // sets a popup on this marker
      .addTo(map);
      this.markers.push(marker);
    }
  }

  updateMarkers(map, index) {
    this.markers[index].setLngLat([
        this.trains[index].location.coordinates[0],
        this.trains[index].location.coordinates[1]
    ]);
    this.markers[index].addTo(map);
  }

  // remove all markers so we can load the new ones in
  removeOldMarkers() {
    //console.log("markers: " + this.markers.length);
    this.markers.forEach(marker => {
      marker.remove();
    });
    this.markers = [];
  }

  addStations(map) {
    this.traisStationService.getStations().subscribe( data => {
      this.stations = data;
      
      for (let i = 0; i < this.stations.length; i++) {
        if(this.stations[i].type == "STATION"  && this.stations[i].passengerTraffic) {
          var popup = new mapboxgl.Popup({ offset: 25 })
          .setHTML(
            '<h5>' + this.stations[i].stationName + '</h5>'
          );
          
          // create DOM element for the marker
          var el = document.createElement('div');
          el.id = 'station';

          var marker = new mapboxgl.Marker(el)
          .setLngLat([
            this.stations[i].longitude,
            this.stations[i].latitude
          ])
          .setPopup(popup) // sets a popup on this marker
          .addTo(map);
        }
      }
    });  
  }
}
