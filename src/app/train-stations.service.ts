import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrainStationsService {

  constructor(private http: HttpClient) {

  }
  
  public getStations(): Observable<any> {
    return this.http.get("https://rata.digitraffic.fi/api/v1/metadata/stations");
  }
}
