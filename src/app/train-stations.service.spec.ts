import { TestBed } from '@angular/core/testing';

import { TrainStationsService } from './train-stations.service';
import { HttpClientModule } from '@angular/common/http';

describe('TrainStationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]
  }));

  it('should be created', () => {
    const service: TrainStationsService = TestBed.get(TrainStationsService);
    expect(service).toBeTruthy();
  });
});
