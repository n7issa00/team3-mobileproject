import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimetableService {

  timetable: any = [];

  constructor(private http: HttpClient) {}

  public getTimetables(departureStation, arrivalStation, departureDate): Observable<any> {
    return this.http.get("https://rata.digitraffic.fi/api/v1/live-trains/station/" + 
    departureStation.stationShortCode + "/" + arrivalStation.stationShortCode + "?departure_date=" + departureDate + "&include_nonstopping=false&limit=20");
  }

  saveTimetable(timetable) {
    this.timetable = timetable;
  }

  

}
