import { Component, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TimetableService } from '../timetable.service';
import { TrainStationsService } from '../train-stations.service';
import { AlertController } from '@ionic/angular';

import { IonicSelectableComponent, IonicSelectableTitleTemplateDirective } from 'ionic-selectable';
import { resetFakeAsyncZone } from '@angular/core/testing';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  departureStation: string;
  arrivalStation: string;
  departureDate: any;
  timetable: any = [];
  stations: any = [];

  constructor(public navCtrl: NavController, private timetableService: TimetableService, private trainStationService: TrainStationsService, private alertController: AlertController) {
    this.departureStation = '';
    this.arrivalStation = '';
    this.departureDate = 0;
  }

  ionViewDidEnter() {
    this.trainStationService.getStations().subscribe( data => {
      this.stations = data;
      console.log(this.stations);
      this.removeNonPassangerStations();
    });
  }

  removeNonPassangerStations(){
    let i = 0;
    for (i = 0; i < this.stations.length; i++) {
      if (this.stations[i].passengerTraffic === false) {
        this.stations.splice(i, 1);
        i--;
      }
    }
    console.log(this.stations);
  }

  

  getTimetable(){
    let reason = '';
    if (this.departureStation === '') {
      reason = 'Departure Station.';
      this.presentAlert(reason);
    }
    if (this.arrivalStation === '') {
      reason = 'Destination Station.';
      this.presentAlert(reason);
    }
    if (this.departureDate === 0) {
      reason = 'Departure Date.';
      this.presentAlert(reason);
    }


    this.timetableService.getTimetables(this.departureStation, this.arrivalStation, this.departureDate).subscribe( data => {
      this.timetable = data;
      //console.log(this.timetable);
      this.timetableService.saveTimetable(this.timetable);
      this.navCtrl.navigateForward('/timetables');
    });
    
  }

  departureChange(event: {
    component: IonicSelectableComponent,
    value: any 
  }) {
    console.log('departureStation:', event.value);
  }
  arrivalChange(event: {
    component: IonicSelectableComponent,
    value: any 
  }) {
    console.log('arrivalStation:', event.value);
  }


  async presentAlert(reason: String) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Choose ' + reason,
      buttons: ['OK']
    });

    await alert.present();
  }
  
  
}
