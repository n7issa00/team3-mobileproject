import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetablesPage } from './timetables.page';

describe('TimetablesPage', () => {
  let component: TimetablesPage;
  let fixture: ComponentFixture<TimetablesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetablesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimetablesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
