import { TestBed, async } from '@angular/core/testing';

import { TrainLocationsService } from './train-locations.service';
import { HttpClientModule } from '@angular/common/http';

describe('TrainLocationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ]
  }));

  it('should be created', () => {
    const service: TrainLocationsService = TestBed.get(TrainLocationsService);
    expect(service).toBeTruthy();
  });

  it('should get data', async(() => {
    const service: TrainLocationsService = TestBed.get(TrainLocationsService);
    service
    .getLocations()
    .subscribe(data => {
      let result: any = [];
      result = data;
      console.log(data);
      expect(result.length).toBeGreaterThan(0);
    });
  }));
});
