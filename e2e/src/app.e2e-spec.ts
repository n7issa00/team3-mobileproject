import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should navigate around site tabs', () => {
    page.navigateTo(); // Navigate to home.
    page.waitPageToLoad('tab1'); // Wait that homepage is loaded.
    page.clickElement('ion-icon: search'); // Navigate to station search page.
    page.waitPageToLoad('tab2'); // Wait that station search is loaded.
    
  });
});
