import { browser, by, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';

export class AppPage {
  /*
  Navigates to specified url
  */
  navigateTo(url: string = '/') {
    return browser.get(url);
  }

  /*
  wait that page is loaded. timeout is 5 seconds
  */
waitPageToLoad(page: string) {
  browser.wait(protractor.ExpectedConditions.urlContains(page), 20000);
}

navigateToTab2(url: string = 'http://localhost:8100/tabs/tab2') {
  return browser.get(url);
}

clickElement(selector: string) {
  return new Promise((resolve) => {
    element(by.css(selector)).click().then(() => {
    });
  });
}
    
  
    


waitPage2ToLoad(page: string) {
  browser.wait(protractor.ExpectedConditions.urlContains(page), 10000);
}

}
